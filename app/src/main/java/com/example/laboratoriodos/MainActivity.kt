package com.example.laboratoriodos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnProcesar.setOnClickListener {
            var cResultado=""
            if(edtYear.text.toString().isEmpty()){
                imgGeneracion.setImageDrawable(null)
                tvResultado.text = cResultado
                Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val nYear=edtYear.text.toString().toInt()
            when(nYear){
                in(1930..1948) -> {
                    cResultado="6,300,000"
                    imgGeneracion.setImageResource(R.drawable.imgausteridad)
                }
                in(1949..1968) -> {
                    cResultado="12,200,000"
                    imgGeneracion.setImageResource(R.drawable.imgambicion)
                }
                in(1969..1980) -> {
                    cResultado="9,300,000"
                    imgGeneracion.setImageResource(R.drawable.imgobsesion)
                }
                in(1981..1993) -> {
                    cResultado="7,200,000"
                    imgGeneracion.setImageResource(R.drawable.imgfrustracion)
                }
                in(1994..2010) -> {
                    cResultado="7,800,000"
                    imgGeneracion.setImageResource(R.drawable.imgirreverencia)
                }
                else ->{
                    cResultado="No perteneces a ninguna generación"
                    imgGeneracion.setImageDrawable(null)
                }
            }
            tvResultado.text="$cResultado"
        }
    }
}